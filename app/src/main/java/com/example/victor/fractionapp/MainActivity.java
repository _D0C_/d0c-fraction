package com.example.victor.fractionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText inputFractionN1, inputFractionN2, inputFractionD1, inputFractionD2;
    EditText outputFractionN, outputFractionD;
    EditText inputSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputFractionN1 = (EditText) findViewById(R.id.input_fraction1_numinator);
        inputFractionD1 = (EditText) findViewById(R.id.input_fraction1_denuminator);

        inputFractionN2 = (EditText) findViewById(R.id.input_fraction2_numinator);
        inputFractionD2 = (EditText) findViewById(R.id.input_fraction2_denuminator);

        outputFractionN = (EditText) findViewById(R.id.output_fraction_nominator);
        outputFractionD = (EditText) findViewById(R.id.output_fraction_denominator);


        inputSign = (EditText) findViewById(R.id.input_sign);
    }


    public void solveExpression(View view) {

        String nom1 = inputFractionN1.getText().toString();
        String den1 = inputFractionD1.getText().toString();

        String nom2 = inputFractionN2.getText().toString();
        String den2 = inputFractionD2.getText().toString();

        Fraction f1 = new Fraction(Integer.valueOf(nom1), Integer.valueOf(den1));
        Fraction f2 = new Fraction(Integer.valueOf(nom2), Integer.valueOf(den2));

        Fraction res = null;

        char sign = inputSign.getText().toString().charAt(0);

        switch (sign) {
            case '+' :
                res = f1.add(f2);
                break;
            case '-' :
                res = f1.sub(f2);
                break;
            case '*' :
                res = f1.mult(f2);
                break;
            case '/' :
                res = f1.div(f2);
                break;
        }

        if (res != null) {
            outputFractionN.setText(String.valueOf(res.getNominator()));
            outputFractionD.setText(String.valueOf(res.getDenominator()));
        } else {
            outputFractionN.setText("Err");
            outputFractionD.setText("Err");
        }

    }
}
