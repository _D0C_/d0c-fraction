package com.example.victor.fractionapp;

/**
 * Created by victor on 27.10.16.
 * Rewrited by D0C.
 */
public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction add (Fraction f) {
     if (denominator != f.denominator){
         int d1=denominator;
         int d2= f.denominator;
         nominator= nominator*d2;
         denominator= denominator*d2;
         f.nominator=f.nominator* d1;
         f.denominator= f.denominator*d1;
     }
        if (denominator != 0) {
            return new Fraction(nominator + f.nominator, denominator);
        }else return null;
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction sub(Fraction f) {
        if (denominator != f.denominator){
            int d1=denominator;
            int d2= f.denominator;
            nominator= nominator*d2;
            denominator= denominator*d2;
            f.nominator=f.nominator* d1;
            f.denominator= f.denominator*d1;
        }
        if (denominator != 0) {
            return new Fraction(nominator - f.nominator, denominator);
        } else return null;
    }

    public Fraction mult( Fraction f2) {
        if (denominator*f2.denominator != 0) {
            return new Fraction(nominator*f2.nominator, denominator*f2.denominator);
        }
        else return null;
    }

    public Fraction div(Fraction f2) {
        if (denominator*f2.nominator != 0) {
            return new Fraction(nominator*f2.denominator, denominator*f2.nominator);
        }
        else return null;
    }
}
